# Question 7

## Description

<details><summary>Click to expand</summary>
Information about pets is kept in two separate tables:

TABLE dogs  
id INTEGER NOT NULL PRIMARY KEY,  
name VARCHAR(50) NOT NULL  

TABLE cats  
id INTEGER NOT NULL PRIMARY KEY,  
name VARCHAR(50) NOT NULL  

Write an SQL query that select all distinct pet names.
</details>

## Live-Test
You can test the query on the following link [https://sqliteonline.com/](https://sqliteonline.com/) by copy-pasting the following commands.

CREATE TABLE Dogs (ID integer NOT NULL PRIMARY KEY, name varchar(50) NOT NULL);  
CREATE TABLE Cats (ID integer NOT NULL PRIMARY KEY, name varchar(50) NOT NULL);  

INSERT INTO Dogs  
(ID, name)  
VALUES  
(1,"Rex"),  
(2,"Mike"),  
(3,"Rex"),  
(4,"Ruffus");  

INSERT INTO Cats  
(ID, name)  
VALUES  
(1,"Mike"),  
(2,"Tyrus"),  
(3,"George"),  
(4,"Fluffy");  

SELECT Name From Dogs  
UNION  
SELECT Name From Cats;  
